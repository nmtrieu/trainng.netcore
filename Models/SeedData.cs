﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MVC_SinhVien.Data;
using System;
using System.Linq;

namespace MVC_SinhVien.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new SinhVienContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<SinhVienContext>>()))
            {
                // Look for any movies.
                if (context.SinhVien.Any())
                {
                    return;
                }

                context.SinhVien.AddRange(
                    new SinhVien
                    {
                        Id = 01,
                        Name = "Triều",
                        DiaChi = "Quận 7",
                        GioiTinh = "Nam ",
                    },

                    new SinhVien
                    {
                        Id=02,
                        Name = "Oanh",
                        DiaChi = "Quận 9",
                        GioiTinh = "Nữ",
                    }
                );
                context.SaveChanges();
            }
        }
    }
}