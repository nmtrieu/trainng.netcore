﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MVC_SinhVien
{
    public class PageList<S> : List<S>
    {
        public int PageIndex { get; private set; }
        public int TotalPage { get; private set; }
        public PageList(List<S> items, int count, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            TotalPage = (int)Math.Ceiling(count / (double)pageSize);

            this.AddRange(items);
        }
        public bool HPPage
        {
            get
            {
                return (PageIndex > 1);
            }
        }
        public bool NextPage
        {
            get
            {
                return (PageIndex < TotalPage);
            }
        }
        public static async Task<PageList<S>> GetSAsync(IQueryable<S> source, int pageIndex, int pageSize)
        {
            var count = await source.CountAsync();
            var items = await source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
            return new PageList<S>(items, count, pageIndex, pageSize);
        }
    }
}
