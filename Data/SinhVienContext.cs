﻿using Microsoft.EntityFrameworkCore;
using MVC_SinhVien.Models;

namespace MVC_SinhVien.Data
{
    public class SinhVienContext : DbContext
    {
        public SinhVienContext(DbContextOptions<SinhVienContext> options)
            : base(options)
        {
        }

        public DbSet<SinhVien> SinhVien { get; set; }
    }
}